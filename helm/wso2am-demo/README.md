wso2am-demo
===========
A Helm chart for wso2am-demo with MySQL database

Current chart version is `1.0.0`





## Chart Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts[0].host | string | `"chart-example.local"` |  |
| ingress.hosts[0].paths | list | `[]` |  |
| ingress.tls | list | `[]` |  |
| mysqlDeploy.replicaCount | int | `1` |  |
| mysqlImage.image.pullPolicy | string | `"IfNotPresent"` |  |
| mysqlImage.image.repository | string | `"mysql"` |  |
| mysqlImage.image.tag | float | `5.7` |  |
| mysqlPorts.port | int | `3306` |  |
| mysqlVolumeClaims.mysqlClaimCapacity | string | `"2Gi"` |  |
| mysqlVolumeClaims.mysqlClaimName | string | `"mysql-pv-claim"` |  |
| namespaces[0] | string | `"wso2am-demo-mysql"` |  |
| resources | object | `{}` |  |
| wso2amConfig.am_db_name | string | `"amdb_1"` |  |
| wso2amConfig.dbpassword | string | `"root"` |  |
| wso2amConfig.dbuser | string | `"root"` |  |
| wso2amConfig.hostname | string | `"am.demo.com"` |  |
| wso2amConfig.shared_db_name | string | `"wso2db_1"` |  |
| wso2amDeploy.replicaCount | int | `1` |  |
| wso2amImage.image.pullPolicy | string | `"Always"` |  |
| wso2amImage.image.repository | string | `"registry.gitlab.com/sagaragu.dev/wso2am-demo"` |  |
| wso2amImage.image.tag | string | `"latest"` |  |
| wso2amImage.imagePullSecrets | string | `"regsecret"` |  |
| wso2amImage.version | string | `"wso2am-3.0.0"` |  |
| wso2amPorts.gwHTTPNodePort | int | `32230` |  |
| wso2amPorts.gwHTTPPort | int | `8280` |  |
| wso2amPorts.gwHTTPSNodePort | int | `32627` |  |
| wso2amPorts.gwHTTPSPort | int | `8243` |  |
| wso2amPorts.mgtHTTPSNodePort | int | `32294` |  |
| wso2amPorts.mgtHTTPSPort | int | `9443` |  |
| wso2amVolumeClaims.executionplansClaimName | string | `"am-executionplans-storage-pv-claim"` |  |
| wso2amVolumeClaims.executionplansClaimNameCapacity | string | `"1Gi"` |  |
| wso2amVolumeClaims.synapseConfigClaimCapacity | string | `"1Gi"` |  |
| wso2amVolumeClaims.synapseConfigClaimName | string | `"am-synapse-configs-storage-pv-claim"` |  |
| wso2amdemo.mysqlNamespace | string | `"wso2am-demo-mysql"` |  |
| wso2amdemo.wso2amNamespace | string | `"wso2am-demo"` |  |
