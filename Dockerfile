
# --gitlab copy --
FROM wso2/wso2am:3.0.0
COPY build/conf/* /home/wso2carbon/wso2am-3.0.0/repository/conf/
COPY build/apis/* /home/wso2carbon/wso2-tmp/synapse-configs/default/api/
COPY build/lib/* /home/wso2carbon/wso2am-3.0.0/repository/components/lib/


